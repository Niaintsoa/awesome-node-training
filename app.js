const express = require('express');
const app = express();

const PORT = 3000;

app.set('views', './views');
app.set('view engine', 'ejs');

app.get('/movies', (req, res) => {
    // res.send('Many movies here');
    res.render('movies');
});

app.get('/movie/add', (req, res) => {
    res.send('Add movie here');
});

app.get('/movie/:id', (req, res) => {
    const id = req.params.id;
    res.send(`Movie number ${id}`);
});


app.get('/', (req, res) => {
    // res.send('Hello World');
    res.render('index');
});

app.listen(PORT, () => {
    console.log(`App listen on port ${PORT}`);
});